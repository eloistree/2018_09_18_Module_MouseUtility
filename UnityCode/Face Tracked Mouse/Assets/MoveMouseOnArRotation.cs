﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveMouseOnArRotation : MonoBehaviour
{


    public Transform m_direction;
    public Transform m_rootDirection;
    public Vector3 m_euler;
    public Vector3 m_foward;
    public float sign;
    public float horizontalAngle;
    public float verticalAngle;
    public float pctX;
    public float pctY;

    public float minAngle = -30, maxAngle = 30;
    public float minVerticalAngle = -10, maxVerticalAngle = 5;
    public float rangeAngle = 20;

    public Vector3 m_defaultOrientaiton = new Vector3(90, 0, 0);
    public float m_lerpSpeed = 0.4f;

    public bool m_useLerp = true;


    float pctClampX;

    float pctClampY;


    public Text m_caliber;

    public void Caliber() {

        StartCoroutine(StartCalibration());
    }

    private IEnumerator StartCalibration()
    {
        m_caliber.text = "Look at left";
        yield return new WaitForSeconds(3);
        minAngle = horizontalAngle;

        m_caliber.text = "Look at Right";
        yield return new WaitForSeconds(3);
        maxAngle = horizontalAngle;

        m_caliber.text = "Look at Top";
        yield return new WaitForSeconds(3);
        maxVerticalAngle = verticalAngle;

        m_caliber.text = "Look at Down";
        yield return new WaitForSeconds(3);
        minVerticalAngle = verticalAngle;

        m_caliber.text = "";
    }

    void Update()
    {

        m_foward = m_direction.InverseTransformDirection(m_direction.forward);
        sign = m_direction.forward.x < 0 ? -1 : 1;


        Quaternion rotation = m_direction.rotation * Quaternion.Inverse(m_rootDirection.rotation);
        m_euler = rotation.eulerAngles;




        horizontalAngle = (m_euler.y < 180f ? m_euler.y : -(360 - m_euler.y));
        verticalAngle = -(m_euler.x < 180f ? m_euler.x : -(360 - m_euler.x));


        if (MouseUtility.WindowSim.InputDeviceState.IsTogglingKeyInEffect(WindowsInput.Native.VirtualKeyCode.NUMLOCK))
        {
            //float pourcent = 
            //pctX = Mathf.Lerp(pctX, (1f - Mathf.Clamp(horizontalAngle / rangeAngle, -1f, 1f)) / 2f, Time.deltaTime);
            //pctY = Mathf.Lerp(pctY, (1f - Mathf.Clamp(verticalAngle / rangeAngle, -1f, 1f)) / 2f, Time.deltaTime);
            pctClampX = Mathf.Lerp(pctClampX, Mathf.Clamp01((horizontalAngle - minAngle) / (maxAngle - minAngle)), Time.deltaTime  );
            pctClampY = Mathf.Lerp(pctClampY, Mathf.Clamp01((verticalAngle - minVerticalAngle) / (maxVerticalAngle - minVerticalAngle)), Time.deltaTime );

            if (m_useLerp)
            {
                pctX = Mathf.Lerp(pctX, pctClampX, m_lerpSpeed * Time.deltaTime);
                pctY = Mathf.Lerp(pctY, pctClampY, m_lerpSpeed * Time.deltaTime);
            }
            else
            {
                pctX = pctClampX;
                pctY = pctClampY;
            }
            //            pctX =  (1f - Mathf.Clamp(horizontalAngle / rangeAngle, -1f, 1f)) / 2f;
            //            pctY =  (1f - Mathf.Clamp(verticalAngle / rangeAngle, -1f, 1f)) / 2f;



            MouseUtility.MovePointerInGlobalViewTo(pctX, pctY);
        }

    }
    // Hello my friend ;)

}
