﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_MonitorsSystem : MonoBehaviour {

    public AspectRatioFitter m_monitorsSystemRatio;
    public RectTransform m_monitorsSystem;
    public RectTransform m_cursor;

    public GameObject m_monitorPrefab;

    [Header("Auto Generated")]
    public List<UI_Monitor> m_monitorsPanel;

    [Header("Debug")]
    public Text m_inputInfo;

    public DisplayConfiguration m_display;

    // Use this for initialization
    void Start()
    {
        
        InvokeRepeating("Refresh", 0, 1);
    }
    

    void Refresh()
    {
        m_display = MouseUtility.Monitors.Configuration;
        if (m_display == null)
            return;



        float ratio = m_display.GetRatio();
        ratio = ratio > 10f ? 1f : ratio;
        ratio = ratio < 0.1f ? 1f : ratio;
        float width = m_display.GetWidth();
        float height = m_display.GetHeight();
        int monitorsCount = m_display.GetMonitorsCount();
        Vector2 cursorviewport = MouseUtility.Monitors.UnityMonitor.GetCursorViewport();
        Vector2 cursorPourcentPosition = MouseUtility.Monitors.UnityMonitor.GetCursorViewport();

        m_monitorsSystemRatio.aspectRatio = ratio;
        
        SetCursorPosition(cursorviewport.x, cursorviewport.y);
        IMonitor[] monitors = m_display.GetMonitors();
        CheckForScreens(monitors.Length);
        for (int i = 0; i < monitors.Length; i++)
        {
            SetLocalWindowInGlobal(m_display.GetWidth(), m_display.GetHeight(), monitors[i], m_monitorsPanel[i].GetRectTransform());
        }

        for (int i = 0; i < monitors.Length; i++)
        {
            UI_Monitor monitor = m_monitorsPanel[i].GetComponent<UI_Monitor>();
            monitor.SetMonitorInfo(monitors[i]);

        }

    }

    private void CheckForScreens(int screenNumbers)
    {
        while (m_monitorsPanel.Count < screenNumbers)
        {

            GameObject newPanel = GameObject.Instantiate(m_monitorPrefab);
            newPanel.transform.parent = m_monitorsSystem.transform;
            newPanel.SetActive(true);
            m_monitorsPanel.Add(newPanel.GetComponent<UI_Monitor>());
        }
    }

    private void SetCursorPosition(float x, float y)
    {
        m_cursor.anchorMin = new Vector2(x, y);
        m_cursor.anchorMax = new Vector2(x, y);
        m_cursor.anchoredPosition = new Vector2(0.5f, 0.5f);
        m_cursor.sizeDelta = new Vector2(10, 10);
        m_cursor.pivot = new Vector2(0.5f, 0.5f);
    }
    private void SetLocalWindowInGlobal(float widht, float height, IMonitor local, RectTransform localTrans)
    {
        Vector2 leftBottom = (local.GetBottomLeft());
        Vector2 rightTop = (local.GetTopRight());
        float lbx, lby;
        float rtx, rty;
        lbx = leftBottom.x / widht;
        rtx = rightTop.x / widht;
        lby = leftBottom.y / height;
        rty = rightTop.y / height;

        localTrans.anchorMin = new Vector2(lbx, lby);
        localTrans.anchorMax = new Vector2(rtx, rty);
        localTrans.anchoredPosition = new Vector2(0.5f, 0.5f);
        localTrans.sizeDelta = new Vector2(0, 0);
        localTrans.pivot = new Vector2(0.5f, 0.5f);
    }


}
