﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Monitor : MonoBehaviour {

    public IMonitor m_monitor;
    public Text m_resolutionInfo;
    public Text m_displayName;

    // Use this for initialization
    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            Refresh();
        }
    }
    


    public void SetMonitorInfo(IMonitor monitor)
    {
        m_monitor = monitor;
        Refresh();
    }

    private void Refresh()
    {
        if (m_monitor == null)
            return;
        m_resolutionInfo.text = string.Format("{0:0}x{1:0}", m_monitor.GetWidth(), m_monitor.GetHeight());
        m_displayName.text = m_monitor.GetName();
    }

    internal RectTransform GetRectTransform()
    {
        return GetComponent<RectTransform>();
    }
}
