﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_CaptureMonitor : MonoBehaviour {

    public InputField m_monitorName;
    public InputField m_displayIndex;
    public Text m_countDown;
    public Text m_warningMessage;

    public MonitorsSystemBuilder monitorsBuilder = new MonitorsSystemBuilder();
    public DisplayConfiguration finalConfiguration;


    public void Start()
    {
        m_monitorName.onValueChanged.AddListener(NameChanged);
        m_displayIndex.onValueChanged.AddListener(IndexChanged);
    }

    public int GetIndex()
    {
        return int.Parse(m_displayIndex.text)-1;
    }

    private void IndexChanged(string index)
    {
        IConfigurableMonitor monitor = monitorsBuilder.GetMonitorByIndex(GetIndex());
        m_monitorName.text = (monitor == null || string.IsNullOrEmpty(monitor.GetName()) ? ("Monitor " + index) : monitor.GetName());
    }

    private void NameChanged(string monitorName)
    {
        IConfigurableMonitor monitor = monitorsBuilder.GetMonitorByIndex(GetIndex());
        monitor.SetName(monitorName);
    }

    public void Increment()
    {
        int index = int.Parse(m_displayIndex.text);
        index++;
        m_displayIndex.text = "" + index;
    }
    public void Decrement()
    {
        int index = int.Parse(m_displayIndex.text);
        index--;
        if (index < 0)
            index = 0;
        m_displayIndex.text = "" + index;
    }


    public void StartRecordingMonitor()
    {
        StopCoroutine(StartRecording());
        StartCoroutine(StartRecording());
    }

    private IEnumerator StartRecording()
    {

        IConfigurableMonitor monitor = monitorsBuilder.GetMonitorByIndex(GetIndex());
        monitor.Reset();
        monitor.SetName(m_monitorName.text);
        m_warningMessage.text = "Record start in 3 seconds";
        m_countDown.text = "3";
        yield return new WaitForSeconds(1);
        m_countDown.text = "2";
        yield return new WaitForSeconds(1);
        m_countDown.text = "1";
        yield return new WaitForSeconds(1);
        m_warningMessage.text = "Record ! Move your mouse in the monitor";
        recordPosition = true;
        m_countDown.text = "10";
        yield return new WaitForSeconds(5);
        m_countDown.text = "5";
        yield return new WaitForSeconds(1);
        m_countDown.text = "4";
        yield return new WaitForSeconds(1);
        m_countDown.text = "3";
        yield return new WaitForSeconds(1);
        m_countDown.text = "2";
        yield return new WaitForSeconds(1);
        m_countDown.text = "1";
        yield return new WaitForSeconds(1);
        m_countDown.text = "Rec";
        recordPosition = false;
        m_warningMessage.text = string.Format("Recorded,{0}: {1}x{2} ", monitor.GetName(), monitor.GetWidth(), monitor.GetHeight());
        DisplayConfiguration configuration = ConvertBuilderToConfiguration(monitorsBuilder);
        finalConfiguration = configuration;
        MouseUtility.Monitors.SetNewConfiguration(configuration);
    }

    private DisplayConfiguration ConvertBuilderToConfiguration(MonitorsSystemBuilder monitorsBuilder)
    {
        DisplayConfiguration config = new DisplayConfiguration();
        int monitorCount = monitorsBuilder.GetMonitorsCount();
        config.m_monitors = new FixedMonitor[monitorCount];
        IMonitor frame = monitorsBuilder.GetFrameView();
        for (int i = 0; i < monitorCount; i++)
        {
            IMonitor monitor = monitorsBuilder.GetMonitorByIndex(i);
            config.m_monitors[i] = new FixedMonitor();
            config.m_monitors[i].m_name = monitor.GetName();
            Vector2 relativePosition = frame.ConvertAbsoluteToRelative(monitor.GetBottomLeft());
            config.m_monitors[i].m_x = relativePosition.x;
            config.m_monitors[i].m_y = relativePosition.y;
            config.m_monitors[i].m_width = monitor.GetWidth();
            config.m_monitors[i].m_height = monitor.GetHeight();
        }
        return config;

    }
    

    public bool recordPosition;

    public void Update()
    {

        monitorsBuilder.GetFrameView().AddPoint(Input.mousePosition.x, Input.mousePosition.y);
        if (recordPosition)
        {
            IMonitorsSystem system = monitorsBuilder;
            system.GetFrameView().AddPoint(Input.mousePosition.x, Input.mousePosition.y);
            monitorsBuilder.GetMonitorByIndex(GetIndex()).AddPoint(Input.mousePosition.x, Input.mousePosition.y);


        }
    }
}
