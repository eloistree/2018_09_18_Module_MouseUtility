﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDrawLine : MonoBehaviour {

    public Transform m_orientation;
    public float m_time = 60;
    public float m_distance = 5;
    public Color m_color= Color.green;
    void Update () {
        
        Debug.DrawLine(m_orientation.position, m_orientation.position + m_orientation.forward * m_distance,m_color, m_time );

		
	}
    private void Reset()
    {
        m_orientation = this.transform;
    }
}
