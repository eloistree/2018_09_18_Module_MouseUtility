﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using WindowsInput;

public class MouseUtility  {

    private static InputSimulator m_simulatorInstance;
    private static IMouseSimulator m_mouseSimulator;
    public static InputSimulator WindowSim
    {
        get
        {
            GenerateInstance();
            return m_simulatorInstance;
        }
    }

    private static void GenerateInstance()
    {
        if (m_simulatorInstance == null)
            m_simulatorInstance = new InputSimulator();
        if (m_mouseSimulator == null)
            m_mouseSimulator = m_simulatorInstance.Mouse;
    }

    public static IMouseSimulator WindowMouse
    {
        get
        {
            GenerateInstance();
            return m_mouseSimulator;
        }
    }



    public static void MovePointerInMainViewTo(double x, double y)
    {
        y = 1f - y;
        //Debug.Log(Screen.currentResolution.width + " x " + Screen.currentResolution.height);
        //Debug.Log(x * Screen.currentResolution.width + " x " + y * Screen.currentResolution.height);
        x = x * Screen.currentResolution.width * 65535 / Screen.currentResolution.width;
        y = y * Screen.currentResolution.height * 65535 / Screen.currentResolution.height;
        WindowMouse.MoveMouseTo(x, y);
    }
    public static void MovePointerInGlobalViewTo(double xPct, double yPct)
    {
        yPct = 1f - yPct;
        WindowMouse.MoveMouseToPositionOnVirtualDesktop(65535*xPct, 65535 *yPct);
    }

    internal static void MovePointerInMonitorTo(int monitorIndex, float xPct, float yPct)
    {
        throw new NotImplementedException();
    }

    public static void GetPointerPosition(int x, int y) {
        if (!Application.runInBackground)
            throw new Exception("Must be run In background to work");
        throw new System.NotImplementedException();
    }

    public static class Monitors {
        private static DisplayConfiguration m_configuration = null;
        public static DisplayConfiguration Configuration { get {
                if (m_configuration == null)
                    LoadConfiguration();
                return m_configuration; } }

        private static UnityMonitorListener m_monitorListener = null;
        public static UnityMonitorListener UnityMonitor
        {
            get {
                if(m_monitorListener==null)
                {
                  m_monitorListener= GameObject.FindObjectOfType<UnityMonitorListener>();
                }
                if (m_monitorListener == null) {
                    GameObject monitorListener = new GameObject("#MonitorListener");
                    m_monitorListener = monitorListener.AddComponent<UnityMonitorListener>();
                }
                return m_monitorListener;
            }

        }

        private static string dataPath = Application.persistentDataPath;
        private static string fullDataPath = Application.persistentDataPath + "/DisplayConfiguration.json";


        public static void SaveConfiguration() {

            if (m_configuration == null)
                return;
            string json = m_configuration.GetJson();
            Debug.Log("< " + fullDataPath);
            File.WriteAllText(fullDataPath, json);
        }
        public static void LoadConfiguration() {
            Debug.Log("> " + fullDataPath);
            string monitorsCompressed = "";
            if (!File.Exists(fullDataPath))
                return ;
            monitorsCompressed = File.ReadAllText(fullDataPath);
            DisplayConfiguration system = DisplayConfiguration.CreateFromJson(monitorsCompressed);
            m_configuration = system;

            if (system != null)
                Debug.Log("Loaded");
            else Debug.Log("Not loader");
        }
        public static void SetNewConfiguration(DisplayConfiguration configuration) {
            if (configuration != null) {
                m_configuration = configuration;
                SaveConfiguration();
            }
        }
    }
  

}



public interface IMonitor {

    string GetName();
    float GetHeight();
    float GetWidth();
    float GetMonitorRatio();
   
    bool  IsPositionInMonitor(float absolutX, float absolutY);
    bool  GetPourcentInMonitor(float absolutX, float absolutY, out float xPourcent, out float yPourcent);
    Vector2 GetCenterPosition();
    Vector2 GetBottomLeft();
    Vector2 GetBottomRight();
    Vector2 GetTopLeft();
    Vector2 GetTopRight();

    Vector2 ConvertAbsoluteToRelative(float absolutX,  float absolutY);
    Vector2 ConvertAbsoluteToRelative(Vector2 absolut);
}

public interface IConfigurableMonitor : IMonitor {

    void SetName(string name);
    void AddPoint(float absolutX, float absolutY);
    void Reset();
}

public interface IMonitorsSystem
{
    Vector2 GetCursorPosition();
    Vector2 GetCursorPourcentPosition();


    IConfigurableMonitor GetFrameView();
    IConfigurableMonitor GetMonitorByIndex(int index);
    IConfigurableMonitor[] GetMonitors();

    int GetMonitorsCount();
    float GetRatio();
    float GetHeight();
    float GetWidth();

    string GetJsonSave();
    void LoadWith(IMonitorsSystem system);
    void Reset();
}





[System.Serializable]
public class MonitorsSystemBuilder : IMonitorsSystem
{
    public string m_systemName="Default";
    public List<UnityMonitorZone> m_monitors = new List<UnityMonitorZone>();
    public UnityMonitorZone m_frameMonitor = new UnityMonitorZone();
    public bool m_Lock;

    public Vector2 GetCursorPosition()
    {
        float x, y;
        x = Input.mousePosition.x;
        y = Input.mousePosition.y;

        if (Input.touchCount > 0)
            x = Input.touches[0].position.x;
        if (Input.touchCount > 0)
            y = Input.touches[0].position.y;

       
        return new Vector2(x, y);
    }

    public Vector2 GetCursorPourcentPosition()
    {
        float x, y;
        Vector2 cursor = GetCursorPosition();
        m_frameMonitor.GetPourcentInMonitor(cursor.x, cursor.y, out x, out y);
        return new Vector2(x, y);
    }

    public IConfigurableMonitor GetFrameView()
    {
        return m_frameMonitor;
    }

    public IConfigurableMonitor GetMonitorByIndex(int index)
    {
        if (index < 0 )
            return null;
        CheckForMonitors(index+1);

        return m_monitors[index];
    }
    private void CheckForMonitors(int screenNumbers)
    {
        while (m_monitors.Count < screenNumbers)
            m_monitors.Add(new UnityMonitorZone());
    }

    public IConfigurableMonitor[] GetMonitors()
    {
        return m_monitors.ToArray();
    }

    public int GetMonitorsCount()
    {
        return m_monitors.Count;
    }

    public float GetRatio()
    {
        return GetWidth() / GetHeight();
    }

    public float GetWidth()
    {
        return m_frameMonitor.GetWidth();
    }
    public float GetHeight()
    {
        return m_frameMonitor.GetHeight();
    }

    public static IMonitorsSystem CreateFromJson(string json)
    {
        try
        {
            //TODO BAD CODE:
            return JsonUtility.FromJson<MonitorsSystemBuilder>(json);
        }
        catch (Exception) { }
        return null;

    }
    public void LoadWith(IMonitorsSystem system)
    {
        throw new System.NotImplementedException();

    }
    public string GetJsonSave()
    {
        return JsonUtility.ToJson(this);
    }

    public void Reset()
    {
        m_frameMonitor.Reset();
        m_monitors.Clear();
    }
    
}


[System.Serializable]
public class UnityMonitorZone : IConfigurableMonitor
{
    // 0,0-----x>
    // |
    // |
    // |Y
    // V
    public string m_name = "";

    [Header("Monitor Value")]
    public Vector2 m_relativeValue= new Vector2();
    [Header("Unity Anchor")]
    public Vector2 m_unityTopLeft = new Vector2();
    public Vector2 m_unityTopRight = new Vector2();
    public Vector2 m_unityBotLeft = new Vector2();
    public Vector2 m_unityBotRight = new Vector2();
    public bool m_undefined = true;

    public UnityMonitorZone()
    {
    }

    public void Reset() {
        m_name = "";
        m_relativeValue = new Vector2();
        m_unityTopLeft = new Vector2();
        m_unityTopRight = new Vector2();
        m_unityBotLeft = new Vector2();
        m_unityBotRight = new Vector2();
        m_undefined = true;

    }

    public void AddPoint(float x, float y)
    {
        if (m_undefined)
        {
            m_unityTopLeft.x = x;
            m_unityTopLeft.y = y;
            m_unityBotRight.x = x;
            m_unityBotRight.y = y;
            m_undefined = false;
        }
        if (x < m_unityTopLeft.x)
            m_unityTopLeft.x = x;
        if (y > m_unityTopLeft.y)
            m_unityTopLeft.y = y;
        if (x > m_unityBotRight.x)
            m_unityBotRight.x = x;
        if (y < m_unityBotRight.y)
            m_unityBotRight.y = y;

        m_unityBotLeft = new Vector2(m_unityTopLeft.x, m_unityBotRight.y);
        m_unityTopRight = new Vector2(m_unityBotRight.x, m_unityTopLeft.y);

        m_relativeValue = new Vector2(Mathf.Abs(m_unityTopLeft.x - m_unityBotRight.x), Mathf.Abs(m_unityTopLeft.y - m_unityBotRight.y));
    }
    public float GetHeight()
    {
        return m_relativeValue.y;
    }
    public float GetWidth()
    {
        return m_relativeValue.x;
    }



    public Vector2 GetRightBottomPositionIn(IMonitor global)
    {
        return GetBottomRight() - global.GetBottomLeft();
    }
    public Vector2 GetRightTopPositionIn(IMonitor global)
    {
        return GetTopRight() - global.GetBottomLeft();
    }
    public Vector2 GetLeftTopPositionIn(IMonitor global)
    {
        return GetTopLeft() - global.GetBottomLeft();
    }
    public Vector2 GetLeftBotPositionIn(IMonitor global)
    {
        return GetBottomLeft() - global.GetBottomLeft();
    }

    public bool IsPositionInMonitor(float absolutX, float absolutY)
    {
        float pctX, pctY;
        GetPourcentInMonitor(absolutX, absolutY, out pctX, out pctY);
        return (pctX >= 0f && pctX <= 1f && pctY >= 0f && pctY <= 1f);

    }

    public bool GetPourcentInMonitor(float absolutX, float absolutY, out float xPourcent, out float yPourcent)
    {
        Vector2 posRelative = ConvertAbsoluteToRelative(absolutX, absolutY);
        xPourcent = posRelative.x / GetWidth() ;
        yPourcent = posRelative.y / GetHeight();
        return xPourcent >= 0f && xPourcent <= 1f && yPourcent >= 0f && yPourcent <= 1f;

    }

    public float GetMonitorRatio()
    {
        return GetWidth() / GetHeight();
    }

    public Vector2 GetCenterPosition()
    {
        //TO CHECK
        return (m_unityBotRight + m_unityTopLeft) / 2f;
    }

    public Vector2 GetBottomLeft()
    {
        return m_unityBotLeft;
    }

    public Vector2 GetBottomRight()
    {
        return m_unityBotRight;
    }

    public Vector2 GetTopLeft()
    {
        return m_unityTopLeft;
    }

    public Vector2 GetTopRight()
    {
        return m_unityTopRight;
    }

    public string GetName()
    {
        return m_name;
    }

    public void SetName(string name)
    {
        m_name = name;
    }

    public  Vector2 ConvertAbsoluteToRelative(float x, float y)
    {
        Vector2 v = new Vector2();
        ConvertToRelative(x, y, out v.x, out v.y);
        return v;
    }

    public Vector2 ConvertAbsoluteToRelative(Vector2 absolutPosition)
    {
        return ConvertAbsoluteToRelative(absolutPosition.x, absolutPosition.y);
    }

    public void ConvertToRelative(float abstractX, float abstractY, out float relativeX, out float relativeY)
    {
        float minX = m_unityTopLeft.x, maxX = m_unityBotRight.x;
        float minY = m_unityBotRight.y, maxY = m_unityTopLeft.y;

        relativeX = abstractX - minX;
        relativeY = abstractY - minY;

    }
}

[System.Serializable]
public class DisplayConfiguration {

    public FixedMonitor[] m_monitors = new FixedMonitor[0];
    public float GetWidth() {
        float width = 0;
        for (int i = 0; i < m_monitors.Length; i++)
        {
            float distance = m_monitors[i].m_x + m_monitors[i].m_width;
            if (distance > width) width = distance;
        }
        return width;
    }
    public float GetHeight() {
        float height = 0;
        for (int i = 0; i < m_monitors.Length; i++)
        {
            float distance = m_monitors[i].m_y + m_monitors[i].m_height;
            if (distance > height) height = distance;
        }
        return height;
    }

    internal static DisplayConfiguration CreateFromJson(string json)
    {
        DisplayConfiguration config = new DisplayConfiguration();
        try
        {
            config = JsonUtility.FromJson<DisplayConfiguration>(json);
        }
        catch (Exception)
        {
            Debug.LogWarning("Can't parse Json !");
        }

        return config;
    }
    public  string GetJson() {
        return JsonUtility.ToJson(this);
    }

    internal float GetRatio()
    {
      return  GetWidth() / GetHeight();
    }

    internal int GetMonitorsCount()
    {
       return m_monitors.Length;
    }

    internal IMonitor[] GetMonitors()
    {
        return m_monitors;
    }

}

 [System.Serializable]
public class FixedMonitor : IMonitor {
    public string m_name;
    public float m_x;
    public float m_y;
    public float m_height;
    public float m_width;

    public Vector2 ConvertAbsoluteToRelative(float x, float y)
    {
        return new Vector2(x - m_x, y - m_y);
    }

    public Vector2 ConvertAbsoluteToRelative(Vector2 absolutValue)
    {
        return ConvertAbsoluteToRelative(absolutValue.x, absolutValue.y);
    }

    public Vector2 GetBottomLeft()
    {
        return new Vector2(m_x, m_y);
    }

    public Vector2 GetBottomRight()
    {
        return new Vector2(m_x+m_width, m_y);
    }

    public Vector2 GetCenterPosition()
    {
        return new Vector2(m_x +m_width/2f, m_y+m_height/2f);
    }

    public float GetHeight()
    {
        return m_height;
    }

    public float GetMonitorRatio()
    {
       return  m_width / m_height;
    }

    public string GetName()
    {
        return m_name;
    }

    public bool GetPourcentInMonitor(float absolutX, float absolutY, out float xPourcent, out float yPourcent)
    {
        Vector2 relative= ConvertAbsoluteToRelative(absolutX, absolutY);
        xPourcent = relative.x / m_width;
        yPourcent = relative.y / m_height;
        return xPourcent >= 0f && xPourcent <= 1f && yPourcent >= 0f && yPourcent <= 1f;
    }

    public Vector2 GetTopLeft()
    {
        return new Vector2(m_x , m_y + m_height );
    }

    public Vector2 GetTopRight()
    {
        return new Vector2(m_x + m_width , m_y + m_height);
    }

    public float GetWidth()
    {
        return m_width;
    }

    public bool IsPositionInMonitor(float x, float y)
    {
        float pctX, pctY;
        return GetPourcentInMonitor(x, y, out pctX, out pctY);
    }
}