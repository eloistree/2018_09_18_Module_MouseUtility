﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityMonitorListener : MonoBehaviour {

    public UnityMonitorZone m_monitorFrame = new UnityMonitorZone();

    public float m_unityScreenWidth;
    public float m_unityScreenHeight;
    public float m_unityScreenPositionX;
    public float m_unityScreenPositionY;


    public UnityEvent m_onScreenSizeChange;
    public UnityEvent m_onScreenPositionChange;
    public Vector3 m_screenPoint;


    public DisplayConfiguration m_displayConfiguration;
 
    
	void Update ()
    {
        float width = Screen.width;
        float height = Screen.height;
        if (m_unityScreenWidth != width || m_unityScreenHeight!=height)
        {
            m_unityScreenWidth = width;
            m_unityScreenHeight = height;
            m_onScreenSizeChange.Invoke();
            m_monitorFrame.Reset();
            Recalibrate();
        }

        bool hasMoved = HasScreenMoved();
        if (hasMoved)
            Recalibrate();

        UpdateMousePosition();

    }

    private bool HasScreenMoved()
    {
        //FIND A WAY TO SAY IT MOVED (USER32)
        return false;
    }

    internal Vector2 GetCursorViewport()
    {
        Vector2 pos = GetRelativePosition();
        pos.x = pos.x / m_monitorFrame.GetWidth();
        pos.y = pos.y / m_monitorFrame.GetHeight();
        return pos;
    }

    internal Vector2 GetRelativePosition()
    {
        return m_monitorFrame.ConvertAbsoluteToRelative(GetCursorPosition());

    }

    internal IMonitor GetFrameView()
    {
        return m_monitorFrame;
    }

    public void Recalibrate() {
        m_displayConfiguration = MouseUtility.Monitors.Configuration;
        if (m_recalibration != null)
            StopCoroutine(m_recalibration);
        m_recalibration = StartCoroutine(Recalibration());

    }
    private Coroutine m_recalibration;
    private IEnumerator Recalibration()
    {
        yield return new WaitForSeconds(1f);

        for (float i = 0; i < 1f; i+=0.1f)
        {
            MouseUtility.MovePointerInGlobalViewTo(i, 0f);
            yield return new WaitForEndOfFrame();
            RecPosition();
            MouseUtility.MovePointerInGlobalViewTo(i, 1f);
            yield return new WaitForEndOfFrame();
            RecPosition();
            MouseUtility.MovePointerInGlobalViewTo(0f, i);
            yield return new WaitForEndOfFrame();
            RecPosition();
            MouseUtility.MovePointerInGlobalViewTo(1f, i);
            yield return new WaitForEndOfFrame();
            RecPosition();
            m_unityScreenPositionX = m_monitorFrame.m_unityBotLeft.x;
            m_unityScreenPositionY = m_monitorFrame.m_unityBotLeft.y;

        }
        yield return new WaitForEndOfFrame();
        MouseUtility.MovePointerInMainViewTo(0.5f, 0.5f);

    }

    private void RecPosition()
    {
        m_screenPoint = GetCursorPosition();
        m_monitorFrame.AddPoint(m_screenPoint.x, m_screenPoint.y);
    }

    private void UpdateMousePosition()
    {
        m_screenPoint = GetCursorPosition();
        m_monitorFrame.AddPoint(m_screenPoint.x, m_screenPoint.y);

    }

    public Vector2 GetCursorPosition()
    {
        float x, y;
        x = Input.mousePosition.x;
        y = Input.mousePosition.y;

        if (Input.touchCount > 0)
            x = Input.touches[0].position.x;
        if (Input.touchCount > 0)
            y = Input.touches[0].position.y;


        return new Vector2(x, y);
    }
}
